---
date: "2019-05-05T00:00:00+01:00"
draft: false
linktitle: Develop DCT
menu:
  example:
    parent: DCT development
    weight: 1
title: Constructs and definitions
toc: true
type: docs
weight: 1
---

A primer on constructs, definitions, and validity will be provided here.
