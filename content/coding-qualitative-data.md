---
layout: single-overview
description: An overview of the instructions for coding qualitative data
#header:
#  caption: null
#  caption_url: null
#  image: novelist.jpg
title: Coding Qualitative Data
url: /coding-qualitative-data/
instructiontype: instructions to code qualitative data as informative about a construct
instructiontypeCapitalized: Instructions to code qualitative data as informative about a construct
dctField: aspect_code
psycoreImage: dct_aspect-coding.svg
---

This overview shows the instructions for coding measurement instruments for all constructs in this repository.
