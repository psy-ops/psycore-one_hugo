---
bio: I'm a researcher in psychology, specifically health psychology, and more specifically, behavior change, often applied to nightlife-related risk behaviors. I enjoy working in the intersection of behavior change, methodology and statistics, and ICT.
email: ""
interests:
- Psychology
- Constructs
- Behavior Change
organizations:
- name: Open University of the Netherlands
  url: https://ou.nl
role: Assistant Professor
# social:
# - icon: envelope
#   icon_pack: fas
#   link: /#contact
# - icon: twitter
#   icon_pack: fab
#   link: https://twitter.com/GeorgeCushen
# - icon: graduation-cap
#   icon_pack: fas
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
# - icon: github
#   icon_pack: fab
#   link: https://github.com/gcushen
# - icon: linkedin
#   icon_pack: fab
#   link: https://www.linkedin.com/
superuser: true
title: Gjalt-Jorn Peters
user_groups:
- Principal Investigators
---

I'm a researcher in psychology, specifically health psychology, and more specifically, behavior change, often applied to nightlife-related risk behaviors. I enjoy working in the intersection of behavior change, methodology and statistics, and ICT.
